"use strict";

exports.toModel = entity => {
    const model = {
        _id: entity._id,
        userName: entity.userName,
        phoneNumber: entity.phoneNumber,
        email: entity.email,
        deviceToken: entity.deviceToken,
        updatedOn: entity.updatedOn,
        createdOn: entity.createdOn,
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};