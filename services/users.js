const build = async (model, context) => {
    const log = context.logger.start('services:users:build')
    if (!model.userName || model.userName === 'string') {
        throw new Error('username is required')
    }
    const user = await new db.user(model)
    log.end()
    return user

}

const createOrGetUser = async (model, context) => {
    const log = context.logger.start('services:users:createOrGetUser')
    const user = await db.user.findOne({ email: { $eq: model.email } });
    if (user) {
        user.deviceToken = model.deviceToken || ''
        user.save()
        log.end()
        return user
    }
    else {
        const user = await build(model, context)
        log.end();
        user.save();
        return user
    }
}

exports.createOrGetUser = createOrGetUser