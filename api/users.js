'use strict'
const service = require('../services/users')
const response = require('../exchange/response')
const mapper = require('../mappers/user')

const createOrGetUser = async (req, res) => {
    const log = req.context.logger.start(`api:users:createOrGetUser`)
    try {
        const data = await service.createOrGetUser(req.body, req.context)
        const message = 'createOrGetUser'
        log.end()
        return response.success(res, message, data)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, 'unable to create Or Get')

    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api:users:get:${req.params.id}`)

    try {
        const user = await service.get(req.params.id, req.context)
        log.end()
        return response.data(res, mapper.toModel(user))
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.createOrGetUser = createOrGetUser
exports.get = get

