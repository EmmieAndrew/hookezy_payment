"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const user = mongoose.Schema({
    userId: { type: String, default: "", required: true },
    userName: { type: String, default: "", required: false },
    email: { type: String, default: "", required: true },
    phoneNumber: { type: String, default: "" },
    // profilePic: { type: String, default: "" },
    type: { type: String, default: "" },
    status: { type: String, default: "active" },
    avatar: { type: String, default: "" },
    lastLoggedIn: { type: Date, default: "" },
    deviceToken: { type: String, default: "" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },

});

mongoose.model("user", user);
module.exports = user;