module.exports = [{
    name: 'userCreate',
    properties: {
        userId: {
            type: 'string'
        },
        userName: {
            type: 'string'
        },
        email: {
            type: 'string'
        },
    }
}]
