module.exports = [
    {
        name: "payCreate",
        properties: {
            userId: {
                type: "string"
            },
            planId: {
                type: "string"
            },
            amount: {
                type: "string"
            },
            currency: {
                type: "string"
            },
            source: {
                type: "string"
            }
        }
    }
];
