module.exports = [{
    url: '/createOrGet',
    post: {
        summary: 'create Or get',
        description: 'create Or get',
        parameters: [{
            in: 'body',
            name: 'body',
            description: 'Model of user creation',
            required: true,
            schema: {
                $ref: '#/definitions/userCreate'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },

},

]
